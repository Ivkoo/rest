import {Component, OnInit} from '@angular/core';

export class Book {

    constructor(
        public id: number,
        public name: string
    ) {
    }
}

@Component({
    selector: 'app-list-books',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    books = [
        new Book(1, 'javainuse'),
        new Book(3, 'fountainhead')
    ];

    constructor() {
    }

    ngOnInit() {
    }
}
