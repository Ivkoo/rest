import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiService} from "../core/api.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  invalidLogin: boolean = false;
  isLogged: boolean = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) {
    if (window.localStorage.getItem('token') != null ) {
      console.log(window.localStorage.getItem('token'));
      this.router.navigate(['list-user']);
    }
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    const loginPayload = {
      name: this.loginForm.controls.name.value,
      password: this.loginForm.controls.password.value
    };
    this.apiService.login(loginPayload).subscribe(data => {
    debugger;
      if (data.status === 200) {
        window.localStorage.setItem('token', data.result.token);
        this.isLogged = true;
        this.router.navigate(['list-user']);
      } else {
        this.invalidLogin = true;
        console.log(data.message);
      }
    });
  }

  ngOnInit() {
    window.localStorage.removeItem('token');
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required]
    });
  }


}
