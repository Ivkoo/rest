import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { User } from "../model/user.model";
import { ApiService } from "../core/api.service";

@Component({
    selector: 'app-admin-page',
    templateUrl: './admin-page.component.html',
    styleUrls: ['./admin-page.component.css']
})

export class AdminPageComponent implements OnInit {

    // @ViewChild('readOnlyTemplate') readOnlyTemplate: TemplateRef<any>;
    // @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
    roles: Array<any>;
    status: Array<any>;
    editedUser: User;
    users: Array<User>;
    isNewRecord: boolean;
    selectedRow: number;
    searchText: string;

    constructor(private adminService: ApiService) {
        this.users = new Array<User>();
    }

    ngOnInit() {
        this.loadUsers();
        this.roles = ['admin', 'user'];
    }

    editUser(user: User) {
      this.adminService.updateUser(user);
    }

    // loadTemplate(user: User) {
    //     if (this.editedUser && this.editedUser.id === user.id) {
    //         return this.editedUser.editTemplate;
    //     } else {
    //         return this.readOnlyTemplate;
    //     }
    // }

    saveUser() {
        if (this.isNewRecord) {
            this.adminService.createUser(this.editedUser).pipe(first()).subscribe(data => {
                this.loadUsers();
            });
            this.isNewRecord = false;
            this.editedUser = null;
        } else {
            this.adminService.updateUser(this.editedUser).pipe(first()).subscribe(data => {
                this.loadUsers();
            });
            this.editedUser = null;
        }
    }

    cancel() {
        if (this.isNewRecord) {
            this.users.pop();
            this.isNewRecord = false;
        }
        this.editedUser = null;
    }

    deleteUser(user: User) {
        this.adminService.deleteUser(user.id);
        this.loadUsers();
    }

    setClickedRow(index: number) {
        this.selectedRow = index;
    }

    private loadUsers() {
        // this.adminService.getUsers().pipe(first()).subscribe((data: User[]) => {
        //     this.users = data;
        // });
    }

}

