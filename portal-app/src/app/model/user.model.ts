export class User {
  id: number;
  name: string;
  surname: string;
  email: string;
  password: string;
  phone: string;
  address: string;
  amount: number;
}
