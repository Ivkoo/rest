import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiService} from "../core/api.service";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  submitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) {
  }

  addForm: FormGroup;

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: [],
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(28)]],
      surname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(28)]],
      email: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(28)]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(32)]],
      phone: ['', [Validators.required, Validators.minLength(13), Validators.maxLength(13)]],
      address: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(28)]],
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.apiService.createUser(this.addForm.value)
      .subscribe(data => {
        this.router.navigate(['list-user']);
      });
  }

  get getFormControls() {
    return this.addForm.controls;
  }
}
