package app.sequrity.jwt;

import org.springframework.security.core.AuthenticationException;

/**
 * JWT Authentication exception for rest-ss application.
 *
 * @author Serhii Ivko
 */
public class JwtAuthenticationException extends AuthenticationException {
    public JwtAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }

    public JwtAuthenticationException(String msg) {
        super(msg);
    }
}
