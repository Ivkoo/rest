package app.utils.converters.impl;

import app.model.User;
import app.utils.dto.UserViewDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converter class for conversion User object to UserViewDto object.
 *
 * @author Serhii Ivko
 */
@Component
public class UserViewDtoToUserConverter implements Converter<User, UserViewDto> {

    @Override
    public UserViewDto convert(User user) {
        UserViewDto dto = new UserViewDto();
        dto.id(user.getId());
        dto.name(user.getName());
        dto.surname(user.getSurname());
        dto.email(user.getEmail());
        dto.phone(user.getPhone());
        dto.password(user.getPassword());
        dto.address(user.getAddress());
        dto.roles(user.getRoles());
        dto.amount(user.getAmount());
        return dto;
    }
}
