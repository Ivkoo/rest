package app.utils.converters.impl;

import app.model.User;
import app.utils.dto.UserCreateDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converter class for conversion UserCreateDto object to User object.
 *
 * @author Serhii Ivko
 */
@Component
public class UserFromUserCreateDtoConverter implements Converter<UserCreateDto, User> {

    @Override
    public User convert(UserCreateDto createDto) {
        User user = new User();
        user.setId(createDto.id());
        user.setName(createDto.name());
        user.setSurname(createDto.surname());
        user.setEmail(createDto.email());
        user.setPassword(createDto.password());
        user.setPhone(createDto.phone());
        user.setAddress(createDto.address());
        user.setAmount(createDto.amount());
        user.setRoles(createDto.roles());
        return user;
    }
}
