package app.utils.dto;

import lombok.Data;

/**
 * DTO class for authentication (login) request.
 *
 * @author Serhii Ivko
 */
@Data
public class AuthenticationRequestDto {
    private String name;
    private String password;
}
