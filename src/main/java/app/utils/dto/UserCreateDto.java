package app.utils.dto;

import app.model.Role;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

/**
 * DTO class for User, that put in database.
 *
 * @author Serhii Ivko.
 * @serial 4467045737823141965L.
 */
@EqualsAndHashCode
@ToString
@Accessors(fluent = true)
public class UserCreateDto implements Serializable {

    private static final long serialVersionUID = 4467045737823141965L;
    @Getter
    private Long id;
    @Getter
    private String name;
    @Getter
    private String surname;
    @Getter
    private String email;
    @Getter
    private String password;
    @Getter
    private String phone;
    @Getter
    private String address;
    @Getter
    private Set<Role> roles;
    @Getter
    private BigDecimal amount;

    public UserCreateDto(String name,
                         String surname,
                         String email,
                         String password,
                         String phone,
                         String address,
                         Set<Role> roles,
                         BigDecimal amount) {
        this(null, name, surname, email, password, phone, address, roles, amount);
    }

    public UserCreateDto(Long id,
                         String name,
                         String surname,
                         String email,
                         String password,
                         String phone,
                         String address,
                         Set<Role> roles,
                         BigDecimal amount) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.address = address;
        this.roles = roles;
        this.amount = amount;
    }
}
