package app.utils.dto;

import app.model.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

/**
 * DTO class for User, that get from database for display on view.
 *
 * @author Serhii Ivko.
 * @serial 6248280764454928434L.
 */
@Data
@Accessors(fluent = true)
@EqualsAndHashCode
public class UserViewDto implements Serializable {
    private static final long serialVersionUID = 6248280764454928434L;
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String phone;
    private String address;
    private Set<Role> roles;
    private BigDecimal amount;
}
