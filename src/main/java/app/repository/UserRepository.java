package app.repository;

import app.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * JPA User interface using JpaRepository.
 *
 * @author Serhii Ivko
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>, CustomUserRepo {
    /**
     * Get user from database by username field.
     *
     * @param name parameter name for search user by name.
     * @author Serhii Ivko
     */
    User findByName(String name);

    /**
     * Get users list from database using native HQL query.
     *
     * @author Serhii Ivko
     */
    @Query("from User")
    List<User> getAllUsers();

    /**
     * Get user from database using native HQL query.
     *
     * @param id user's ID for search in database.
     * @author Serhii Ivko
     */
    @Query("from User u where u.id = :id")
    User getUserById(Long id);
}
