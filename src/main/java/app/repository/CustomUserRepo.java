package app.repository;

import app.model.User;

import java.util.List;

/**
 * JPA custom User interface.
 *
 * @author Serhii Ivko
 */
public interface CustomUserRepo {

    List<User> findAll();
}
