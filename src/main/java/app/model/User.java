package app.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Simple JavaBean domain object representing an user.
 *
 * @author Serhii Ivko
 */
@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
public class User {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    @NotEmpty
    @Size(min = 2, max = 30)
    private String name;

    @Column
    @NonNull
    @NotEmpty
    @Size(min = 2, max = 30)
    private String surname;

    @Column
    @NonNull
    @NotEmpty
    @Email
    private String email;

    @Column
    @NonNull
    @NotEmpty
    private String password;

    @Column
    @NonNull
    @NotEmpty
    @Size(min = 13, max = 13)
    private String phone;

    @Column(length = 400)
    @NonNull
    @NotEmpty
    @Size(min = 3, max = 300)
    private String address;

    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "role", joinColumns = @JoinColumn(name = "id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

    @Column
    private BigDecimal amount;
}
