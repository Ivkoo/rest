package app.model;

/**
 * Enumeration that represents roles of domain objects - USER, ADMIN.
 *
 * @author Serhii Ivko
 */
public enum Role {
    USER,
    ADMIN
}