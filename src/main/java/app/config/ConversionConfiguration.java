package app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.converter.Converter;

import java.util.Set;

/**
 * The class {@code ConversionConfiguration} configure java bean ConversionServiceFactoryBean,
 * that use for conversion between entities.
 *
 * @author Serhii Ivko
 */
@Configuration
public class ConversionConfiguration {

    /**
     * Get a new ConversionServiceFactoryBean and set converters.
     *
     * @param converters is Set of objects Converter type.
     */
    @Bean
    ConversionServiceFactoryBean conversionService(Set<Converter> converters) {
        ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
        bean.setConverters(converters);
        return bean;
    }
}