package app.config;

/**
 * The class {@code ConstantsContainer} contains static constant variables,
 * that use in other application classes and methods.
 *
 * @author Serhii Ivko
 */
public class ConstantsContainer {

    public final static int MAX_INPUT_LENGTH = 28;

    /**
     * The regular expression pattern for validation email.
     */
    public final static String EMAIL_VALIDATION_PATTERN = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b";

    /**
     * The regular expression pattern for validation phone number.
     */
    public final static String PHONE_VALIDATION_PATTERN = "^(?:[+][0-9]{2}\\s?[0-9]{3}[-]?[0-9]{3,}|(?:[(][0-9]{3}[)]|" +
            "[0-9]{3})\\s*[-]?\\s*[0-9]{3}[-][0-9]{4})(?:\\s*x\\s*[0-9]+)?";
}
