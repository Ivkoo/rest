package app.exceptions;

/**
 * {@code ValidationException} is the class that extends from
 * ApplicationException that provide messages of validation errors.
 *
 * @author Serhii Ivko
 * @serial -6744238085152756863L
 */
public class ValidationException extends ApplicationException {
    private static final long serialVersionUID = -6744238085152756863L;

    /**
     * Constructs a new ValidationException with {@code null} as its
     * detail message.
     */
    public ValidationException(String message) {
        super(message);
    }
}
