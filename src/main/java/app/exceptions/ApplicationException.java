package app.exceptions;

/**
 * {@code ApplicationException} is the class of those
 * exceptions that can be thrown during the normal operation of the
 * application.
 *
 * @author Serhii Ivko
 * @serial -2387336791594295898L
 */
public class ApplicationException extends RuntimeException {
    private static final long serialVersionUID = -2387336791594295898L;

    /**
     * Constructs a new ApplicationException with {@code String} message as its
     * detail message.
     */
    public ApplicationException(String message) {
        super(message);
    }

    /**
     * Constructs a new ApplicationException with {@code String} and {@code Throwable} as its
     * detail message and cause of exception.
     */
    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
