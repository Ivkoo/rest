package app.controller;

import app.model.User;
import app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * The class {@code UserController} that handles all URL's.
 *
 * @author Serhii Ivko
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Displays the specified user.
     *
     * @param id the ID of the user to display.
     * @return a ResponseEntity in request body in JSON with the model attributes for the view.
     */
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUserById(@PathVariable("id") Long id) {
        return userService.getUser(id)
                .map(user -> ResponseEntity.ok().body(user))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Displays all users.
     *
     * @return a ResponseEntity with the list users in JSON format.
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> getAllUsers() {
        return Optional
                .of(userService.getUsers())
                .map(user -> ResponseEntity.ok().body(user))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Save new User in database.
     *
     * @param user          new object User with validated fields.
     * @param bindingResult for correct error handling if credentials incorrect.
     * @return a ResponseEntity in JSON with the model attributes for the view.
     */
    @PostMapping
    public ResponseEntity<User> addNewUser(@Valid @RequestBody User user, BindingResult bindingResult) {
        User savedUser;
        if (bindingResult.hasErrors()) {
            return (ResponseEntity<User>) bindingResult.getAllErrors();
        } else {
            savedUser = userService.saveUser(user);
        }
        return ResponseEntity.ok(savedUser);
    }

    /**
     * Update exists User in database.
     *
     * @param newUser       new object User with validated fields for replace old data.
     * @param id            User ID, which must be updated.
     * @param bindingResult for correct error handling if credentials incorrect.
     * @return a ResponseEntity in JSON with the model attributes for the view.
     */
    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(@Valid @RequestBody User newUser, @PathVariable("id") Long id, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return (ResponseEntity<User>) bindingResult.getAllErrors();
        }
        userService.update(newUser, id);
        return ResponseEntity.ok(newUser);
    }

    /**
     * Delete user from database.
     *
     * @param id User ID, which must be deleted.
     * @return a ResponseEntity with the list users in JSON format.
     */
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") Long id) {
        if (userService.isExistUserInDatabaseByID(id)) {
            userService.deleteUser(id);
        }
        return ResponseEntity.ok().build();
    }

    //HQL examples for return user or list users from database.
//    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<User> getUserByIdHQLQuery(@PathVariable("id") Long id) {
//        User user = userService.getUserByIdHQL(id);
//        return ResponseEntity.ok().body(user);
//    }
//
//    @GetMapping
//    public ResponseEntity<List<User>> getAllUsersHQLQuery() {
//        return Optional
//                .of(userService.getAllUsersHQL())
//                .map(user -> ResponseEntity.ok().body(user))
//                .orElseGet(() -> ResponseEntity.notFound().build());
//    }
}
