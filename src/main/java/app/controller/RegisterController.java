package app.controller;

import app.model.User;
import app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/users/signup")
public class RegisterController {
    private UserService userService;

    @Autowired
    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Save new User in database.
     *
     * @param user          new object User with validated fields.
     * @param bindingResult for correct error handling if credentials incorrect.
     * @return a ResponseEntity in JSON with the model attributes for the view.
     */
    @PostMapping
    public ResponseEntity<User> registerUser(@Valid @RequestBody User user, BindingResult bindingResult) {
        User savedUser;
        if (bindingResult.hasErrors()) {
            return (ResponseEntity<User>) bindingResult.getAllErrors();
        } else {
            savedUser = userService.saveUser(user);
        }
        return ResponseEntity.status(200).body(savedUser);
    }
}
