package app.controller;

import app.model.User;
import app.sequrity.jwt.JwtTokenProvider;
import app.service.UserService;
import app.utils.dto.AuthenticationRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * The class {@code LoginController} that handles login by {"/auth"} URL.
 *
 * @author Serhii Ivko
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/auth")
public class LoginController {
    /**
     * The value is used for authenticate user.
     */
    private final AuthenticationManager authenticationManager;

    /**
     * The value is used for generate JSON web token.
     */
    private final JwtTokenProvider jwtTokenProvider;

    /**
     * The value is used for check user in database and return if exists.
     */
    private final UserService userService;

    /**
     * Construct LoginController object with needed parameters.
     *
     * @param authenticationManager use AuthenticationManager field.
     * @param jwtTokenProvider      use JwtTokenProvider field.
     * @param userService           UserService field.
     */
    @Autowired
    public LoginController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    /**
     * The method login display ResponseEntity if user credentials is correct. If credentials is incorrect, throws
     * BadCredentialsException. If user doesn't exists in database, throws UsernameNotFoundException. Also this method
     * returns JSON web token in response body, that contain username and token.
     *
     * @param requestedUser get valid AuthenticationRequestDto as parameter in request body for authenticate user.
     * @return ResponseEntity object.
     */
    @PostMapping()
    public ResponseEntity login(@Valid @RequestBody AuthenticationRequestDto requestedUser) {
        try {
            String name = requestedUser.getName();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(name, requestedUser.getPassword()));
            User user = userService.getUserByName(name);
            if (user == null) {
                throw new UsernameNotFoundException("User with name: " + name + " not found");
            }
            String token = jwtTokenProvider.createToken(name, user.getRoles());
            Map<Object, Object> response = new HashMap<>();
            response.put("name", name);
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid name or password");
        }
    }
}
