package app.aspect;

/**
 * The interface-marker for custom logging methods use aspect.
 *
 * @author Serhii Ivko
 */
public @interface Loggable {

}