package app.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * The class {@code WebServiceLogger} is aspect class for logging public methods in application.
 *
 * @author Serhii Ivko
 */
@Aspect
@Slf4j
@Configuration
public class WebServiceLogger {

    /**
     * Method for search public methods logging in application.
     * Execution goal - find public methods in package with services.
     * Method use Pointcut annotation for apply execution goal.
     */
    @Pointcut("execution(public * app.service..*(..))")
    public void logMethod() {
    }

    /**
     * Aspect method for logging public methods in application.
     *
     * @param thisJoinPoint is instance of ProceedingJoinPoint class.
     * @return object result, that contains call method name, call method parameters,
     * call method return value and save it in log file.
     */
    @Around("logMethod()")
    public Object logWebServiceCall(ProceedingJoinPoint thisJoinPoint) throws Throwable {
        String methodName = thisJoinPoint.getSignature().getName();
        Object[] methodArgs = thisJoinPoint.getArgs();
        log.debug("Call method: " + methodName + ", with parameters: " + Arrays.toString(methodArgs));
        Object result = thisJoinPoint.proceed();
        log.debug("Method: " + methodName + " returns result: " + result);
        return result;
    }
}