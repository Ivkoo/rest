package app.service.impl;

import app.config.ConstantsContainer;
import app.exceptions.ValidationException;
import app.service.UserValidatorService;
import app.utils.dto.UserCreateDto;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

/**
 * Implementation interface UserValidatorService.
 *
 * @author Serhii Ivko
 */
@Service
public class UserValidatorServiceImpl implements UserValidatorService {

    @Override
    public boolean validateUserCredentials(String name,
                                           String surname,
                                           String email,
                                           String password,
                                           String phone,
                                           String address) {
        return (validateName(name)
                && validateSurname(surname)
                && validateUserEmail(email)
                && validateUserPassword(password)
                && validateUserPhone(phone)
                && validateUserAddress(address));
    }

    @Override
    public boolean isUserDtoValid(UserCreateDto createDto) {
        return validateUserCredentials(
                createDto.name(),
                createDto.surname(),
                createDto.email(),
                createDto.password(),
                createDto.phone(),
                createDto.address());
    }

    private static boolean validateName(String name) {
        boolean correctName = !(name.trim().isEmpty() || name.length() > ConstantsContainer.MAX_INPUT_LENGTH);
        if (!correctName) {
            throw new ValidationException("Incorrect name input");
        }
        return correctName;
    }

    private static boolean validateSurname(String surname) {
        boolean correctSurname = !(surname.trim().isEmpty() || surname.length() > ConstantsContainer.MAX_INPUT_LENGTH);
        if (!correctSurname) {
            throw new ValidationException("Incorrect surname input");
        }
        return correctSurname;
    }

    private static boolean validateUserEmail(String email) {
        boolean correctEmail = !(email.trim().isEmpty() || email.length() > ConstantsContainer.MAX_INPUT_LENGTH);
        if (!correctEmail || !validateEmail(email)) {
            throw new ValidationException("Incorrect email");
        }
        return (correctEmail && validateEmail(email));
    }

    private static boolean validateUserPassword(String password) {
        boolean correctPasswordLength = password.length() > 6 && password.length() < ConstantsContainer.MAX_INPUT_LENGTH;
        boolean correctPassword = !password.trim().isEmpty() && correctPasswordLength;
        if (!correctPassword) {
            throw new ValidationException("Incorrect or weak password!");
        }
        return true;
    }

    private static boolean validateUserPhone(String phone) {
        boolean correctPhoneLength = phone.length() == 13;
        boolean correctPhone = !phone.trim().isEmpty() && correctPhoneLength && validatePhone(phone);
        if (!correctPhone) {
            throw new ValidationException("Incorrect phone");
        }
        return true;
    }

    private static boolean validateUserAddress(String address) {
        boolean correctAddress = !(address.trim().isEmpty());
        if (!correctAddress) {
            throw new ValidationException("Incorrect address input");
        }
        return correctAddress;
    }

    private static boolean validateEmail(String email) {
        return Pattern.compile(ConstantsContainer.EMAIL_VALIDATION_PATTERN).matcher(email).matches();
    }

    private static boolean validatePhone(String phone) {
        return Pattern.compile(ConstantsContainer.PHONE_VALIDATION_PATTERN).matcher(phone).matches();
    }
}
