package app.service.impl;

import app.model.Role;
import app.model.User;
import app.repository.UserRepository;
import app.service.UserService;
import app.utils.dto.UserCreateDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Implementation interface UserService.
 *
 * @author Serhii Ivko
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private UserRepository repository;
    private BCryptPasswordEncoder passwordEncoder;
    private ConversionService conversionService;

    @Autowired
    public UserServiceImpl(@Lazy UserRepository repository, @Lazy BCryptPasswordEncoder passwordEncoder, @Lazy ConversionService conversionService) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
        this.conversionService = conversionService;
    }

    @Override
    public Optional<User> isUserUnique(String email) {
        Optional<User> userOptional = repository.findById(repository.findByName(email).getId());
        if (userOptional.isPresent()) {
            repository.findByName(email);
        }
        return userOptional;
    }

    @Override
    public boolean isExistUserInDatabaseByID(Long id) {
        Optional<User> userFromDb = repository.findById(id);
        return userFromDb.isPresent();
    }

    @Override
    @Transactional
    public User saveUser(User user) {
        UserCreateDto createDto = new UserCreateDto(
                user.getId(),
                user.getName(),
                user.getSurname(),
                user.getEmail(),
                user.getPassword(),
                user.getPhone(),
                user.getAddress(),
                user.getRoles(),
                user.getAmount());
        conversionService.convert(createDto, User.class);
        user.setRoles(Collections.singleton(Role.USER));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setAmount(BigDecimal.valueOf(0.0));
        return repository.save(user);
    }

    @Override
    public List<User> getUsers() {
        return repository.findAll();
    }

    @Override
    public User update(User newUser, Long id) {
        User user = null;
        if (repository.findById(id).isPresent()) {
            user = repository.findById(id).get();
            user.setId(newUser.getId());
            user.setName(newUser.getName());
            user.setSurname(newUser.getSurname());
            user.setEmail(newUser.getEmail());
            user.setPassword(newUser.getPassword());
            user.setPhone(newUser.getPhone());
            user.setAddress(newUser.getAddress());
            user.setRoles(newUser.getRoles());
        }
        newUser = repository.save(user);
        return repository.save(newUser);
    }

    @Override
    public User getUserByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public Optional<User> getUser(Long id) {
        return repository.findById(id);
    }

    @Override
    public void deleteUser(Long id) {
        repository.deleteById(id);
    }

    @Override
    public User getUserByIdHQL(Long id) {
        return repository.getUserById(id);
    }

    @Override
    public List<User> getAllUsersHQL() {
        return repository.getAllUsers();
    }

}
