package app.service;

import app.model.User;
import app.utils.dto.UserCreateDto;

/**
 * Service interface for validation fields class {@link User}.
 *
 * @author Serhii Ivko
 */
public interface UserValidatorService {

    boolean validateUserCredentials(String name,
                                    String surname,
                                    String email,
                                    String password,
                                    String phone,
                                    String address);

    boolean isUserDtoValid(UserCreateDto createDto);
}
