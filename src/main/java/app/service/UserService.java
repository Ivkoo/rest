package app.service;

import app.model.User;

import java.util.List;
import java.util.Optional;

/**
 * Service interface for class {@link User}.
 *
 * @author Serhii Ivko
 */
public interface UserService {

    /**
     * Get User from database by name.
     *
     * @param name specified name of user for search in database.
     * @return object User.
     * @author Serhii Ivko
     */
    User getUserByName(String name);

    /**
     * Save User to database.
     *
     * @param user object User with valid credentials for saving to database.
     * @return object User.
     * @author Serhii Ivko
     */
    User saveUser(User user);

    /**
     * Save User to database.
     *
     * @param id user ID with valid credentials for search in database.
     * @return object Optional generified by User.
     * @author Serhii Ivko
     */
    Optional<User> getUser(Long id);

    /**
     * Get list users from database.
     *
     * @return list users.
     * @author Serhii Ivko
     */
    List<User> getUsers();

    /**
     * Remove user from database.
     *
     * @param id user ID for search in database for removing.
     * @author Serhii Ivko
     */
    void deleteUser(Long id);

    /**
     * Update user in database.
     *
     * @param id      user ID for search in database for replacing.
     * @param newUser object User with updated fields for replace old fields in database.
     * @author Serhii Ivko
     */
    User update(User newUser, Long id);

    /**
     * Check user unique in database.
     *
     * @param email user email field for search in database.
     * @return object Optional.
     * @author Serhii Ivko
     */
    Optional<User> isUserUnique(String email);

    /**
     * Check user unique in database.
     *
     * @param id user ID for search in database.
     * @return boolean value.
     * @author Serhii Ivko
     */
    boolean isExistUserInDatabaseByID(Long id);

    /**
     * Get users list from database using native HQL query.
     *
     * @author Serhii Ivko
     */
    List<User> getAllUsersHQL();

    /**
     * Get user from database using native HQL query.
     *
     * @param id user's ID for search in database.
     * @author Serhii Ivko
     */
    User getUserByIdHQL(Long id);
}
