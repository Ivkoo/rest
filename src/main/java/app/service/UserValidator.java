package app.service;

import app.config.ConstantsContainer;
import app.exceptions.ValidationException;
import app.model.User;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

@Service
public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        if (validateUserCredentials(
                user.getName(),
                user.getSurname(),
                user.getEmail(),
                user.getPassword(),
                user.getPhone(),
                user.getAddress())) {
            errors.getAllErrors();
        }
    }

    boolean validateUserCredentials(String name,
                                    String surname,
                                    String email,
                                    String password,
                                    String phone,
                                    String address) {
        return (validateName(name)
                && validateSurname(surname)
                && validateUserEmail(email)
                && validateUserPassword(password)
                && validateUserPhone(phone)
                && validateUserAddress(address));
    }

    private static boolean validateName(String name) {
        boolean correctName = !(name.trim().isEmpty() || name.length() > ConstantsContainer.MAX_INPUT_LENGTH);
        if (!correctName) {
            throw new ValidationException("Incorrect name input");
        }
        return correctName;
    }

    private static boolean validateSurname(String surname) {
        boolean correctSurname = !(surname.trim().isEmpty() || surname.length() > ConstantsContainer.MAX_INPUT_LENGTH);
        if (!correctSurname) {
            throw new ValidationException("Incorrect surname input");
        }
        return correctSurname;
    }

    private static boolean validateUserEmail(String email) {
        boolean correctEmail = !(email.trim().isEmpty() || email.length() > ConstantsContainer.MAX_INPUT_LENGTH);
        if (!correctEmail || !validateEmail(email)) {
            throw new ValidationException("Incorrect email");
        }
        return (correctEmail && validateEmail(email));
    }

    private static boolean validateUserPassword(String password) {
        boolean correctPasswordLength = password.length() > 6 && password.length() < ConstantsContainer.MAX_INPUT_LENGTH;
        boolean correctPassword = !password.trim().isEmpty() && correctPasswordLength;
        if (!correctPassword) {
            throw new ValidationException("Incorrect or weak password!");
        }
        return true;
    }

    private static boolean validateUserPhone(String phone) {
        boolean correctPhoneLength = phone.length() == 13;
        boolean correctPhone = !phone.trim().isEmpty() && correctPhoneLength && validatePhone(phone);
        if (!correctPhone) {
            throw new ValidationException("Incorrect phone");
        }
        return true;
    }

    private static boolean validateUserAddress(String address) {
        boolean correctAddress = !(address.trim().isEmpty());
        if (!correctAddress) {
            throw new ValidationException("Incorrect address input");
        }
        return correctAddress;
    }

    private static boolean validateEmail(String email) {
        return Pattern.compile(ConstantsContainer.EMAIL_VALIDATION_PATTERN).matcher(email).matches();
    }

    private static boolean validatePhone(String phone) {
        return Pattern.compile(ConstantsContainer.PHONE_VALIDATION_PATTERN).matcher(phone).matches();
    }
}
