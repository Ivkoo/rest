package app.service;

import app.utils.dto.UserCreateDto;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Service
public class UserDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return UserCreateDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserValidator userValidator = new UserValidator();
        UserCreateDto createDto = (UserCreateDto) o;
        if (userValidator.validateUserCredentials(
                createDto.name(),
                createDto.surname(),
                createDto.email(),
                createDto.password(),
                createDto.phone(),
                createDto.address())) {
            errors.getAllErrors();
        }
    }
}