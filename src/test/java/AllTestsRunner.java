import app.controller.LoginControllerTest;
import app.controller.UserControllerTest;
import app.encryptor.SaltPasswordsImplTest;
import app.service.UserValidatorTest;
import app.service.impl.UserServiceImplTest;
import app.service.impl.UserValidatorServiceImplTest;
import app.service.impl.parametrized.ParametrizedTestForNullParams;
import app.service.impl.parametrized.ParametrizedValidatorTestCorrectInput;
import app.service.impl.parametrized.ParametrizedValidatorTestIncorrectInput;
import app.service.rule.ParamValidationExpectExceptionRule;
import app.service.rule.ParamValidationTestWithRule;
import app.service.rule.RuleTest;
import app.service.rule.custom.example.CustomRuleTest;
import app.utils.converters.impl.UserFromUserCreateDtoConverterTest;
import app.utils.converters.impl.UserViewDtoToUserConverterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;



@RunWith(Suite.class)
@SuiteClasses({
        LoginControllerTest.class,
        UserControllerTest.class,
        SaltPasswordsImplTest.class,
        ParametrizedValidatorTestCorrectInput.class,
        ParametrizedValidatorTestIncorrectInput.class,
        ParametrizedTestForNullParams.class,
        UserServiceImplTest.class,
        UserValidatorServiceImplTest.class,
        CustomRuleTest.class,
        ParamValidationExpectExceptionRule.class,
        ParamValidationTestWithRule.class,
        RuleTest.class,
        UserValidatorTest.class,
        UserFromUserCreateDtoConverterTest.class,
        UserViewDtoToUserConverterTest.class
})
public class AllTestsRunner {

}

