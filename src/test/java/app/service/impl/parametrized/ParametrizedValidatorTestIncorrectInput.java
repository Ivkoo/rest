package app.service.impl.parametrized;

import app.exceptions.ValidationException;
import app.service.UserValidatorService;
import app.service.impl.UserValidatorServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

@RunWith(value = Parameterized.class)
public class ParametrizedValidatorTestIncorrectInput {
    private String name;
    private String surname;
    private String email;
    private String password;
    private String phone;
    private String address;

    private UserValidatorService service;

    @Before
    public void setUp() throws Exception {
        service = new UserValidatorServiceImpl();
    }

    public ParametrizedValidatorTestIncorrectInput(String name, String surname, String email, String password, String phone, String address) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    @Parameters(name = "{index}: name = {0}, surname = {1}, email = {2}, password = {3}, phone = {4}, address = {5}")
    public static List<Object> data() {
        return Arrays.asList(new Object[][]{
                {"name", "surname", "emai", "password123", "+380501114477", "Dnipro"},
                {"name", "surname", "email@gmail.com", "123", "+380501114477", "Dnipro"},
                {"name", "surname", "email@gmail.com", "password123", "+38050", "Dnipro"},
                {"", "", "", "", "", ""}
        });
    }

    @Test(expected = ValidationException.class)
    public void testFirstCaseIfAllCredentialsCorrect() {
        Assert.assertThat(service.validateUserCredentials(name, surname, email, password, phone, address), is(true));
    }
}
