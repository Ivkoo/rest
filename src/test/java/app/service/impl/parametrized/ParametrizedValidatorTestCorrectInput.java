package app.service.impl.parametrized;

import app.controller.UserController;
import app.model.Role;
import app.service.UserValidatorService;
import app.service.impl.UserValidatorServiceImpl;
import app.utils.dto.UserCreateDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

@RunWith(value = Parameterized.class)
public class ParametrizedValidatorTestCorrectInput {
    private String name;
    private String surname;
    private String email;
    private String password;
    private String phone;
    private String address;

    @InjectMocks
    private UserController controller;

    private UserValidatorService service;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        MockMvc mvc = MockMvcBuilders.standaloneSetup(controller).build();
        UserCreateDto dto = new UserCreateDto(
                1L,
                "name",
                "surname",
                "email",
                "password",
                "+380502003030",
                "dnipro",
                Collections.singleton(Role.USER),
                BigDecimal.valueOf(0));
        dto.roles();
        service = new UserValidatorServiceImpl();
    }

    public ParametrizedValidatorTestCorrectInput(String name, String surname, String email, String password, String phone, String address) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    @Parameters(name = "{index}: name = {0}, surname = {1}, email = {2}, password = {3}, phone = {4}, address = {5}")
    public static List<Object> data() {
        String nameForCheck = "name";
        String surnameForCheck = "surname";
        String emailForCheck = "email@gmail.com";
        String passwordForCheck = "password123";
        String phoneForCheck = "+380501114477";
        String addressForCheck = "Dnipro";
        return Arrays.asList(new Object[][]{
                {nameForCheck, surnameForCheck, emailForCheck, passwordForCheck, phoneForCheck, addressForCheck}
        });
    }

    @Test
    public void testIfAllCredentialsCorrect() {
        Assert.assertThat(service.validateUserCredentials(name, surname, email, password, phone, address), is(true));
    }
}
