package app.service.impl;

import app.exceptions.ValidationException;
import app.model.Role;
import app.service.UserValidatorService;
import app.utils.dto.UserCreateDto;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.assertTrue;

public class UserValidatorServiceImplTest {

    private UserValidatorService service;
    private UserCreateDto createDto;

    @Test
    public void validateUserCredentialsIfAllCorrect() {
        //GIVEN:
        service = new UserValidatorServiceImpl();
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "password123";
        String phone = "+380501114477";
        String address = "Dnipro";

        //WHEN:
        boolean isCredentialsCorrect = service.validateUserCredentials(name, surname, email, password, phone, address);

        //THEN:
        assertTrue(isCredentialsCorrect);
    }

    @Test(expected = ValidationException.class)
    public void validateUserCredentialsIfIncorrectEmail() {
        //GIVEN:
        service = new UserValidatorServiceImpl();
        String name = "name";
        String surname = "surname";
        String email = "email";
        String password = "password123";
        String phone = "+380501114477";
        String address = "Dnipro";

        //WHEN:
        service.validateUserCredentials(name, surname, email, password, phone, address);
    }

    @Test(expected = ValidationException.class)
    public void validateUserCredentialsIfIncorrectPasswordSoShort() {
        //GIVEN:
        service = new UserValidatorServiceImpl();
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "123";
        String phone = "+380501114477";
        String address = "Dnipro";

        //WHEN:
        service.validateUserCredentials(name, surname, email, password, phone, address);
    }

    @Test(expected = ValidationException.class)
    public void validateUserCredentialsIfIncorrectPasswordSoLong() {
        //GIVEN:
        service = new UserValidatorServiceImpl();
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "123456789123456789123456789123456789";
        String phone = "+380501114477";
        String address = "Dnipro";

        //WHEN:
        service.validateUserCredentials(name, surname, email, password, phone, address);
    }

    @Test(expected = ValidationException.class)
    public void validateUserCredentialsIfIncorrectPhone() {
        //GIVEN:
        service = new UserValidatorServiceImpl();
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "password123";
        String phone = "+3805011";
        String address = "Dnipro";

        //WHEN:
        service.validateUserCredentials(name, surname, email, password, phone, address);
    }

    @Test(expected = ValidationException.class)
    public void validateUserCredentialsIfFieldsIsEmpty() {
        //GIVEN:
        service = new UserValidatorServiceImpl();
        String name = "";
        String surname = "";
        String email = "";
        String password = "";
        String phone = "";
        String address = "";

        //WHEN:
        service.validateUserCredentials(name, surname, email, password, phone, address);
    }

    @Test(expected = NullPointerException.class)
    public void validateUserCredentialsIfAllNull() {
        //GIVEN:
        service = new UserValidatorServiceImpl();
        String name = null;
        String surname = null;
        String email = null;
        String password = null;
        String phone = null;
        String address = null;

        //WHEN:
        service.validateUserCredentials(name, surname, email, password, phone, address);
    }

    @Test
    public void validateUserDto() {
        //GIVEN:
        service = new UserValidatorServiceImpl();
        Long id = 1L;
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "password123";
        String phone = "+380501114477";
        String address = "Dnipro";
        Set<Role> roles = Collections.singleton(Role.USER);
        BigDecimal amount = BigDecimal.valueOf(0);

        //WHEN:
        createDto = new UserCreateDto(id, name, surname, email, password, phone, address, roles, amount);
        boolean isDtoValid = service.isUserDtoValid(createDto);

        //THEN:
        assertTrue(isDtoValid);
    }

    @Test(expected = NullPointerException.class)
    public void validateUserDtoIfNull() {
        //GIVEN:
        createDto = null;

        //WHEN:
        service.isUserDtoValid(createDto);
    }
}