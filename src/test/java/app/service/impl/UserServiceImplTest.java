package app.service.impl;

import app.model.Role;
import app.model.User;
import app.repository.UserRepository;
import app.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase()
public class UserServiceImplTest {
    private User user;
    private Long id = 1L;
    private String name = "name";
    private String surname = "surname";
    private String email = "email@gmail.com";
    private String password = "password123";
    private String phone = "+380501114477";
    private String address = "Dnipro";
    private List<User> users;
    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Mock
    private UserRepository repository;

    @Mock
    private ConversionService conversionService;

    @InjectMocks
    private UserService userService = new UserServiceImpl(repository, passwordEncoder, conversionService);

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        user = new User();
        user.setId(id);
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setAddress(address);
        user.setRoles(Collections.singleton(Role.ADMIN));
        users = new ArrayList<>();
        users.add(user);
    }

    @Test
    public void saveUser() {
        User userFromService = new User();
        userFromService.setId(id);
        userFromService.setName(name);
        userFromService.setSurname(surname);
        userFromService.setEmail(email);
        userFromService.setPassword(password);
        userFromService.setPhone(phone);
        userFromService.setAddress(address);
        userFromService.setRoles(Collections.singleton(Role.ADMIN));
        when(repository.save(any(User.class))).thenReturn(userFromService);
        User saved = userService.saveUser(userFromService);
        Assert.assertEquals(userFromService, saved);
    }

    @Test
    public void getUsers() {
        when(repository.findAll()).thenReturn(users);
        List<User> usersFromDao = userService.getUsers();
        Assert.assertEquals(usersFromDao, users);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void update() {
        when(repository.findByName(name)).thenReturn(users.get(0));
        User userFromDao = userService.getUserByName(name);
        userFromDao.setName("newName");
        userFromDao.setSurname("newSurname");
        userFromDao.setEmail("newEmail@email.com");
        userFromDao.setPassword("newPassword123");
        userFromDao.setPhone("+380507771133");
        userService.update(userFromDao, id);
        User updated = users.get(0);
        Assert.assertEquals("newName", updated.getName());
        Assert.assertEquals("newSurname", updated.getSurname());
        Assert.assertEquals("newEmail@email.com", updated.getEmail());
        Assert.assertEquals("newPassword123", updated.getPassword());
        Assert.assertEquals("+380507771133", updated.getPhone());
    }

    @Test
    public void getUserByName() {
        when(repository.findByName(name)).thenReturn(users.get(0));
        User userFromDao = userService.getUserByName(name);
        Assert.assertEquals(user, userFromDao);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void getUserOptional() {
        when(repository.findById(id)).thenReturn(Optional.of(users.get(0)));
        Optional<User> userOptional = userService.getUser(id);
        Optional<User> user = Optional.of(users.get(0));
        Assert.assertEquals(userOptional, user);
    }

    @Test
    public void deleteUser() {
        userService.deleteUser(id);
        verify(repository, times(1)).deleteById(eq(id));
    }
}