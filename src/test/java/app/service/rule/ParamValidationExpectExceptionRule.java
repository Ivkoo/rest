package app.service.rule;

import app.exceptions.ValidationException;
import app.model.User;
import app.service.UserValidator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.List;

@RunWith(value = Parameterized.class)
public class ParamValidationExpectExceptionRule {
    private String name;
    private String surname;
    private String email;
    private String password;
    private String phone;
    private String address;

    private UserValidator service;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        service = new UserValidator();
    }

    public ParamValidationExpectExceptionRule(String name, String surname, String email, String password, String phone, String address) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    @Parameters(name = "{index}: name = {0}, surname = {1}, email = {2}, password = {3}, phone = {4}, address = {5}")
    public static List<Object> data() {
        return Arrays.asList(new Object[][]{
                {"name", "surname", "emai", "password123", "+380501114477", "Dnipro"},
                {"name", "surname", "email@gmail.com", "123", "+380501114477", "Dnipro"},
                {"name", "surname", "email@gmail.com", "password123", "+38050", "Dnipro"},
                {"", "", "", "", "", ""}
        });
    }

    @Test
    public void testFirstCaseIfAllCredentialsCorrect() {
        thrown.expect(ValidationException.class);
        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setAddress(address);
        service.validate(user, null);
    }
}
