package app.service.rule;

import app.model.User;
import app.service.UserValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class RuleTest {
    private User user;
    private UserValidator validator;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "password123";
        String phone = "+380501114477";
        String address = "Dnipro";
        validator = new UserValidator();
        user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setAddress(address);
    }

    @Test
    public void testIncorrectNameRule() {
        //GIVEN:
        String name = "";
        user.setName(name);
        String incorrectNameInput = "Incorrect name input";
        String expectedName = user.getName();

        //THEN:
        Assert.assertEquals(name, expectedName);
        thrown.expectMessage(incorrectNameInput);

        //WHEN:
        validator.validate(user, null);
    }

    @Test
    public void testIncorrectEmailRule() {
        //GIVEN:
        String email = "email";
        user.setEmail(email);
        String incorrectEmailInput = "Incorrect email";
        String expectedEmail = user.getEmail();

        //THEN:
        Assert.assertEquals(email, expectedEmail);
        thrown.expectMessage(incorrectEmailInput);

        //WHEN:
        validator.validate(user, null);
    }

//    public void test(){
//    //    A a = new a();
//    //    Код для проверки объекта а после вызова эксппшена
//    //    a.methodWithException();
//    }
}
