package app.service.rule.custom.example;

import app.exceptions.ValidationException;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.ArrayList;
import java.util.List;


public class TestRuleImpl implements TestRule {
    private final List<Runnable> runnables = new ArrayList<>();

    @Override
    public Statement apply(final Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    base.evaluate();
                } catch (Exception e) {
                    if (e instanceof ValidationException) {
                        verify();
                        System.out.println("Exception class: " + e.getClass());
                        System.out.println("Exception method description: " + e.getMessage());
                    }
                }
            }
        };
    }

    public void check(Runnable runnable){
        runnables.add(runnable);
    }

    public void verify(){
        runnables.forEach(Runnable::run);
    }
}