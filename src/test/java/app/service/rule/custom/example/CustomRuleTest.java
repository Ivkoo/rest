package app.service.rule.custom.example;

import app.model.User;
import app.service.UserValidator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CustomRuleTest {
    private UserValidator userValidator;
    private User user;

    @Before
    public void setUp() {
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "password123";
        String phone = "+380501114477";
        String address = "Dnipro";
        userValidator = new UserValidator();
        user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setAddress(address);
    }

    @Rule
    public TestRuleImpl rule = new TestRuleImpl();

    @Test
    public void testIncorrectName() {
        user.setName("");
        rule.check(()-> System.out.println("Object " + user.getClass().getName() + " has name: \"" + user.getName() + "\""));
        userValidator.validate(user, null);
    }

    @Test
    public void testIncorrectSurname() {
        user.setSurname("");
        userValidator.validate(user, null);
    }

    @Test
    public void testIncorrectEmail() {
        user.setEmail("email");
        userValidator.validate(user, null);
    }

    @Test
    public void testIncorrectPassword() {
        user.setPassword("00");
        userValidator.validate(user, null);
    }

    @Test
    public void testIncorrectPhone() {
        user.setPhone("698");
        userValidator.validate(user, null);
    }

    @Test
    public void testIncorrectAddress() {
        user.setAddress("");
        userValidator.validate(user, null);
    }
}