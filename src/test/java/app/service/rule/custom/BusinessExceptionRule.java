package app.service.rule.custom;

import app.exceptions.ValidationException;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class BusinessExceptionRule implements TestRule {
    private Object[] parameters;

    public void setParameters(Object... parameters) {
        this.parameters = parameters;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        this.parameters = new Object[0];
        return new SimpleRule(base);
    }

    class SimpleRule extends Statement {
        private final Statement base;

        public SimpleRule(Statement base) {
            this.base = base;
        }

        @Override
        public void evaluate() throws Throwable {
            try {
                base.evaluate();
            } catch (Exception e) {
                if (e instanceof ValidationException) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
