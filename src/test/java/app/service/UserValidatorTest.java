package app.service;

import app.model.Role;
import app.model.User;
import app.utils.dto.UserCreateDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.DataBinder;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserValidatorTest {

    private static final ResourceBundleMessageSource MESSAGE_SOURCE = new ResourceBundleMessageSource();

    static {
        MESSAGE_SOURCE.setBasename("message");
    }

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private UserDtoValidator dtoValidator;

    @Test
    public void validateUserWithCorrectCredentials() {
        final User user = new User();
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "password123";
        String phone = "+380501114477";
        String address = "Dnipro";
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setAddress(address);
        final DataBinder dataBinder = new DataBinder(user);
        dataBinder.addValidators(userValidator);
        dataBinder.validate();
        Assert.assertFalse(dataBinder.getBindingResult().hasErrors());
    }

    @Test
    public void validateUserDtoValidWithCorrectCredentials() {
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "password123";
        String phone = "+380501114477";
        String address = "Dnipro";
        Set<Role> roles = Collections.singleton(Role.USER);
        BigDecimal amount = BigDecimal.valueOf(0);
        final UserCreateDto createDto = new UserCreateDto(1L, name, surname, email, password, phone, address, roles, amount);
        final DataBinder dataBinder = new DataBinder(createDto);
        dataBinder.addValidators(dtoValidator);
        dataBinder.validate();
        Assert.assertFalse(dataBinder.getBindingResult().hasErrors());
    }
}