package app.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("/test.properties")
@Sql(value = {"/create-contact-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-contact-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class LoginControllerTest {

    @Autowired
    WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
    }

    @After
    public void tearDown() throws Exception {
        mockMvc = null;
    }

    @Test
    public void loginWhenEmptyResponse() throws Exception {
        //GIVEN
        String expectedResponse = "";
        int expectedStatus = 405;
        String uri = "/auth";

        //WHEN
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String result = mvcResult.getResponse().getContentAsString();
        String actualResponse = mvcResult.getResponse().getContentAsString();

        //THEN
        assertEquals(actualResponse, expectedResponse);
        assertEquals(expectedStatus, status);
        assertNotNull(result);
    }

    @Test
    public void loginWithAdminCredentials() throws Exception {
        //GIVEN
        String urlPattern = "/auth";
        String content = "{\"name\":\"admin\",\"password\":\"admin\"}";
        RequestBuilder request = MockMvcRequestBuilders
                .post(urlPattern)
                .content(content)
                .contentType(MediaType.APPLICATION_JSON);
        //WHEN
        MvcResult result = mockMvc.perform(request)
                .andExpect(status().isOk()).andReturn();
        int expectedStatus = 200;
        int actualStatus = result.getResponse().getStatus();

        //THEN
        assertEquals(expectedStatus, actualStatus);
        assertNotNull(result);
    }

    @Test(expected = NestedServletException.class)
    public void loginWithWrongCredentials() throws Exception {
        //GIVEN
        String urlPattern = "/auth";
        String content = "{\"name\":\"admin222\",\"password\":\"admin222\"}";
        RequestBuilder request = MockMvcRequestBuilders
                .post(urlPattern)
                .content(content)
                .contentType(MediaType.APPLICATION_JSON);
        //WHEN
        MvcResult result = mockMvc.perform(request)
                .andExpect(status().isOk()).andReturn();
        int expectedStatus = 200;
        int actualStatus = result.getResponse().getStatus();

        //THEN
        assertEquals(expectedStatus, actualStatus);
        assertNotNull(result);
    }
}