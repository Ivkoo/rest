package app.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("/test.properties")
@Sql(value = {"/create-contact-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-contact-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UserControllerTest {

    @Autowired
    WebApplicationContext webApplicationContext;

    private MockMvc mvc;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
    }

    @After
    public void tearDown() throws Exception {
        mvc = null;
    }

    @Test
    public void getUserWhenNotFound() throws Exception {
        //GIVEN
        String expectedResponse = "";
        int expectedStatus = 404;
        String uri = "/users/9";

        //WHEN
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String result = mvcResult.getResponse().getContentAsString();
        String actualResponse = mvcResult.getResponse().getContentAsString();
        System.out.println("status = " + status);
        System.out.println("mvc result = " + mvcResult);
        System.out.println("actual result = " + result);
        System.out.println("actual response = " + actualResponse);

        //THEN
        assertEquals(actualResponse, expectedResponse);
        assertEquals(expectedStatus, status);
        assertNotNull(result);
    }

    @Test
    public void getUserWhenExists() throws Exception {
        //GIVEN
        String uri = "/users/1";
        String expectedResponse = "{\"id\":1," +
                "\"name\":\"admin\"," +
                "\"surname\":\"admin\"," +
                "\"email\":\"bill@gates.com\"," +
                "\"password\":\"$2a$10$bp0KyGybnchzYiiCbOz/s.ofRlcusIkrcVioO2m0Fd1oJtvEYeTyy\"," +
                "\"phone\":\"+380504447788\"," +
                "\"address\":\"Huston\"," +
                "\"roles\":[]," +
                "\"amount\":10.0" +
                "}";
        int expectedStatus = 200;

        //WHEN
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String result = mvcResult.getResponse().getContentAsString();
        String actualResponse = mvcResult.getResponse().getContentAsString();

        //THEN
        assertEquals(actualResponse, expectedResponse);
        assertEquals(expectedStatus, status);
        assertNotNull(result);
    }

    @Test
    public void getAllUsers() throws Exception {
        //GIVEN
        String uri = "/users";
        String expectedResponse = "[{\"id\":1," +
                "\"name\":\"admin\"," +
                "\"surname\":\"admin\"," +
                "\"email\":\"bill@gates.com\"," +
                "\"password\":\"$2a$10$bp0KyGybnchzYiiCbOz/s.ofRlcusIkrcVioO2m0Fd1oJtvEYeTyy\"," +
                "\"phone\":\"+380504447788\"," +
                "\"address\":\"Huston\"," +
                "\"roles\":[]," +
                "\"amount\":10.0" +
                "},{\"id\":2," +
                "\"name\":\"Elon\"," +
                "\"surname\":\"Musk\"," +
                "\"email\":\"elon@gmail.com\"," +
                "\"password\":\"spaceX1983\"," +
                "\"phone\":\"+380504447799\"," +
                "\"address\":\"Huston\"," +
                "\"roles\":[]," +
                "\"amount\":100.0" +
                "}]";
        int expectedStatus = 200;

        //WHEN
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String result = mvcResult.getResponse().getContentAsString();
        String actualResponse = mvcResult.getResponse().getContentAsString();

        //THEN
        assertEquals(actualResponse, expectedResponse);
        assertEquals(expectedStatus, status);
        assertNotNull(result);
    }

    @Test
    public void addNewUser() throws Exception {
        //GIVEN
        String urlPattern = "/users";
        String content = "{\"id\":\"3\"," +
                "\"name\":\"admin\"," +
                "\"surname\":\"admin\"," +
                "\"email\":\"bill@gates.com\"," +
                "\"password\":\"hell12oworld123\"," +
                "\"phone\":\"+380504447788\"," +
                "\"address\":\"Huston\"," +
                "\"roles\":[]," +
                "\"amount\":10.0" +
                "}";
        RequestBuilder request = MockMvcRequestBuilders
                .post(urlPattern)
                .content(content)
                .contentType(MediaType.APPLICATION_JSON);

        //WHEN
        MvcResult result = mvc
                .perform(request)
                .andExpect(status().isOk())
                .andReturn();
        int expectedStatus = 200;
        int actualStatus = result.getResponse().getStatus();

        //THEN
        assertEquals(expectedStatus, actualStatus);
        assertNotNull(result);
    }

    @Test
    public void updateUser() throws Exception {
        //GIVEN
        String urlPattern = "/users/1";
        String content = "{\"id\":1," +
                "\"name\":\"admin\"," +
                "\"surname\":\"admin\"," +
                "\"email\":\"bill@gates.com\"," +
                "\"password\":\"helloworld123\"," +
                "\"phone\":\"+380504447788\"," +
                "\"address\":\"London\"," +
                "\"roles\":[]," +
                "\"amount\":10.0" +
                "}";
        RequestBuilder request = MockMvcRequestBuilders
                .put(urlPattern)
                .content(content)
                .contentType(MediaType.APPLICATION_JSON);

        //WHEN
        MvcResult result = mvc
                .perform(request)
                .andExpect(status().isOk())
                .andReturn();
        int expectedStatus = 200;
        int actualStatus = result.getResponse().getStatus();
        String expectedContent = result.getResponse().getContentAsString();

        //THEN
        assertEquals(expectedStatus, actualStatus);
        assertEquals(content, expectedContent);
        assertNotNull(result);
    }

    @Test
    public void deleteAccount() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders
                .delete("/users/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}