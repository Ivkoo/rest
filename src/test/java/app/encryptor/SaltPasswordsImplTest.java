package app.encryptor;

import app.encryptor.impl.SaltPasswordsImpl;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;

public class SaltPasswordsImplTest {

    private static final Logger LOGGER = Logger.getLogger(SaltPasswordsImplTest.class);

    @Before
    public void setup(){
        BasicConfigurator.configure();
    }

    @Test
    public void testGeSalt() throws Exception {

        SaltPasswords passwords = new SaltPasswordsImpl();
        final byte[] bytes = passwords.getSalt(0);
        int arrayLength = bytes.length;

        assertThat("Expected length is", arrayLength, is(64));
    }

    @Test
    public void testGeSalt32() throws Exception {
        SaltPasswords passwords = new SaltPasswordsImpl();
        final byte[] bytes = passwords.getSalt32();
        int arrayLength = bytes.length;
        assertThat("Expected length is", arrayLength, is(32));
    }

    @Test
    public void testGeSalt64() throws Exception {
        SaltPasswords passwords = new SaltPasswordsImpl();
        final byte[] bytes = passwords.getSalt64();
        int arrayLength = bytes.length;
        assertThat("Expected length is", arrayLength, is(64));
    }

    @Test
    public void testHash() throws Exception {
        SaltPasswords passwords = new SaltPasswordsImpl();
        final byte[] hash = passwords.hash("holacomoestas", passwords.getSalt64());
        assertThat("Array is not null", hash, Matchers.notNullValue());
    }


    @Test
    public void testSHA3() throws UnsupportedEncodingException {
        SHA3.DigestSHA3 md = new SHA3.Digest256();
        md.update("holasa".getBytes("UTF-8"));
        final byte[] digest = md.digest();
        assertThat("expected digest is:",digest,Matchers.notNullValue());
    }

    @Test
    public void testIsExpectedPasswordIncorrect() throws Exception {

        String password = "givemebeer";
        SaltPasswords passwords = new SaltPasswordsImpl();

        final byte[] salt64 = passwords.getSalt64();
        final byte[] hash = passwords.hash(password, salt64);
        //The salt and the hash go to database.

        final boolean isPasswordCorrect = passwords.isExpectedPassword("jfjdsjfsd", salt64, hash);

        assertThat("Password is not correct", isPasswordCorrect, is(false));

    }

    @Test
    public void testIsExpectedPasswordCorrect() throws Exception {
        String password = "givemebeer";
        SaltPasswords passwords = new SaltPasswordsImpl();
        final byte[] salt64 = passwords.getSalt64();
        final byte[] hash = passwords.hash(password, salt64);
        //The salt and the hash go to database.
        final boolean isPasswordCorrect = passwords.isExpectedPassword("givemebeer", salt64, hash);
        assertThat("Password is correct", isPasswordCorrect, is(true));
    }

    @Test
    public void testGenerateRandomPassword() throws Exception {
        SaltPasswords passwords = new SaltPasswordsImpl();
        final String randomPassword = passwords.generateRandomPassword(10);
        LOGGER.info(randomPassword);
        assertThat("Random password is not null", randomPassword, Matchers.notNullValue());
    }
}