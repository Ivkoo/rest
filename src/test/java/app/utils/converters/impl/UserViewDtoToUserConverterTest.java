package app.utils.converters.impl;

import app.model.Role;
import app.model.User;
import app.utils.dto.UserCreateDto;
import app.utils.dto.UserViewDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.core.convert.converter.Converter;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;

@RunWith(JUnit4.class)
public class UserViewDtoToUserConverterTest {

    private User user;
    private UserCreateDto createDto;
    private Converter<User, UserViewDto> converter;

    @Test
    public void convert() {
        //GIVEN:
        converter = new UserViewDtoToUserConverter();
        Long id = 1L;
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "password123";
        String phone = "+380501114477";
        String address = "Dnipro";
        Set<Role> roles = Collections.singleton(Role.USER);
        BigDecimal amount = BigDecimal.valueOf(0);
        user = new User();
        user.setId(id);
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setAddress(address);
        user.setAmount(amount);
        user.setRoles(roles);

        //WHEN:
        createDto = new UserCreateDto(id, name, surname, email, password, phone, address, roles, amount);
        UserViewDto viewDto = converter.convert(user);

        //THEN:
        Assert.assertEquals(viewDto.id(), createDto.id());
        Assert.assertEquals(viewDto.name(), createDto.name());
        Assert.assertEquals(viewDto.surname(), createDto.surname());
        Assert.assertEquals(viewDto.email(), createDto.email());
        Assert.assertEquals(viewDto.password(), createDto.password());
        Assert.assertEquals(viewDto.phone(), createDto.phone());
        Assert.assertEquals(viewDto.address(), createDto.address());
        Assert.assertEquals(viewDto.roles(), createDto.roles());
        Assert.assertEquals(viewDto.amount(), createDto.amount());
    }

    @Test(expected = NullPointerException.class)
    public void convertIfContainsNull() {
        //GIVEN:
        converter = new UserViewDtoToUserConverter();
        Long id = 1L;
        String name = null;
        String surname = null;
        String email = null;
        String password = null;
        String phone = null;
        String address = null;
        Set<Role> roles = Collections.singleton(Role.USER);
        BigDecimal amount = BigDecimal.valueOf(0);
        user = new User();
        user.setId(id);
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setAddress(address);
        user.setAmount(amount);
        user.setRoles(roles);

        //WHEN:
        createDto = new UserCreateDto(id, name, surname, email, password, phone, address, roles, amount);
        UserViewDto viewDto = converter.convert(user);
    }
}