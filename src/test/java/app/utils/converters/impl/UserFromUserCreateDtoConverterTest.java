package app.utils.converters.impl;

import app.model.Role;
import app.model.User;
import app.utils.dto.UserCreateDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.core.convert.converter.Converter;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;

@RunWith(JUnit4.class)
public class UserFromUserCreateDtoConverterTest {
    private Converter<UserCreateDto, User> converter;
    private User actualUser;
    private UserCreateDto createDto;

    @Test
    public void convert() {
        //GIVEN:
        converter = new UserFromUserCreateDtoConverter();
        Long id = 1L;
        String name = "name";
        String surname = "surname";
        String email = "email@gmail.com";
        String password = "password123";
        String phone = "+380501114477";
        String address = "Dnipro";
        Set<Role> roles = Collections.singleton(Role.USER);
        BigDecimal amount = BigDecimal.valueOf(0);
        actualUser = new User();
        actualUser.setId(id);
        actualUser.setName(name);
        actualUser.setSurname(surname);
        actualUser.setEmail(email);
        actualUser.setPassword(password);
        actualUser.setPhone(phone);
        actualUser.setAddress(address);
        actualUser.setAmount(amount);
        actualUser.setRoles(roles);

        //WHEN:
        createDto = new UserCreateDto(id, name, surname, email, password, phone, address, roles, amount);
        User expectedUser = converter.convert(createDto);

        //THEN:
        Assert.assertEquals(actualUser, expectedUser);
    }

    @Test(expected = NullPointerException.class)
    public void convertIfContainsNull() {
        //GIVEN:
        converter = new UserFromUserCreateDtoConverter();
        Long id = 1L;
        String name = null;
        String surname = null;
        String email = null;
        String password = null;
        String phone = null;
        String address = null;
        Set<Role> roles = Collections.singleton(Role.USER);
        BigDecimal amount = BigDecimal.valueOf(0);
        actualUser = new User();
        actualUser.setId(id);
        actualUser.setName(name);
        actualUser.setSurname(surname);
        actualUser.setEmail(email);
        actualUser.setPassword(password);
        actualUser.setPhone(phone);
        actualUser.setAddress(address);
        actualUser.setAmount(amount);
        actualUser.setRoles(roles);

        //WHEN:
        createDto = new UserCreateDto(id, name, surname, email, password, phone, address, roles, amount);
        User expectedUser = converter.convert(createDto);

        //THEN:
        Assert.assertEquals(actualUser, expectedUser);
    }
}