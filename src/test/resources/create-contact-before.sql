drop table users;

create table users
(
    id       int          not null primary key auto_increment,
    name     varchar(25)  not null,
    surname  varchar(25)  not null,
    email    varchar(25)  not null,
    password varchar(400) not null,
    phone    varchar(100) not null,
    address  varchar(50)  not null,
    amount   double       not null
);

insert into users(id, name, surname, email, password, phone, address, amount)
values (1, 'admin', 'admin', 'bill@gates.com', '$2a$10$bp0KyGybnchzYiiCbOz/s.ofRlcusIkrcVioO2m0Fd1oJtvEYeTyy',
        '+380504447788', 'Huston', 10.00);
insert into users(id, name, surname, email, password, phone, address, amount)
values (2, 'Elon', 'Musk', 'elon@gmail.com', 'spaceX1983', '+380504447799', 'Huston', 100.00);